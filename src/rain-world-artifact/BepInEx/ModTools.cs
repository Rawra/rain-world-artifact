﻿using BepInEx;
using BepInEx.Bootstrap;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RWArtifact.BepInEx
{
    public sealed class ModTools
    {
        public static bool IsModLoaded(string modGUID)
        {
            foreach (var p in Chainloader.PluginInfos)
            {
                if (p.Value.Metadata.GUID == modGUID)
                    return true;
            }
            return false;
        }

        public static bool TryGetMod(string modGUID, out PluginInfo plugin)
        {
            plugin = default;
            foreach (var p in Chainloader.PluginInfos)
            {
                if (p.Value.Metadata.GUID == modGUID)
                {
                    plugin = p.Value;
                    return true;
                }
            }
            return false;
        }
    }
}
