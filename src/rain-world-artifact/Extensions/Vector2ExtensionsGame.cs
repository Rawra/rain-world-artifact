﻿using RWArtifact.Compat;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Numerics;
namespace RWArtifact.Extensions
{
    public static partial class Vector2Extensions
    {
        /// <summary>
        /// Convert a given world-coordinate (origin: bottom left) to a screen coordinate (origin: top left of window)
        /// screen position = world position - camera position.
        /// 
        /// Supports sharpener resolution mod
        /// </summary>
        /// <param name="worldPoint"></param>
        /// <param name="camera"></param>
        /// <returns></returns>
        public unsafe static Vector2 WorldToScreen(this ref UnityEngine.Vector2 worldPoint, ref readonly RoomCamera camera)
        {
            //Globals.mls.LogInfo($"WorldToScreen: wP={worldPoint.ToString()}, c.sS={camera.sSize.ToString()}, c.pos={camera.pos.ToString()}, sR={Globals.sharpenerResolution.ToString()}");

            // Prepare the args
            Vector2* worldPointPtr = (Vector2*)Unsafe.AsPointer(ref worldPoint);
            Vector2* camPosPtr = camera.pos.ToNumericsVector2PtrByRef();

            // Get the current game resolution and camera position
            Vector2* gameResolution;
            float scaleX, scaleY;
            if (CompatSharpener.SharpenerEnabled)
            {
                gameResolution = CompatSharpener.SharpenerResolution.ToNumericsVector2PtrByRef();

                // Calculate the scaling factor based on the difference between the current game resolution and the original game resolution
                scaleX = (float)gameResolution->X / CompatSharpener.SharpenerFakeRes.x;
                scaleY = (float)gameResolution->Y / CompatSharpener.SharpenerFakeRes.y;
            }
            else
            {
                gameResolution = camera.sSize.ToNumericsVector2PtrByValue();
                scaleX = 1.00f;
                scaleY = 1.00f;
            }

            // Apply scaling to the world point
            Vector2 scaledWorldPoint;
            scaledWorldPoint.X = worldPoint.x * scaleX;
            scaledWorldPoint.Y = worldPoint.y * scaleY;

            // Apply scaling to the camera position
            Vector2 scaledCameraPosition;
            scaledCameraPosition.X = camera.pos.x * scaleX;
            scaledCameraPosition.Y = camera.pos.y * scaleY;

            // Transform scaled world point to screen coordinates
            Vector2 screenPoint = scaledWorldPoint - scaledCameraPosition;
            screenPoint.Y = gameResolution->Y - screenPoint.Y;
            return screenPoint;

            /*Vector2 screenPlayerPoint = Vector2.Zero;
            screenPlayerPoint.X = worldPoint.x - camera.pos.x;
            screenPlayerPoint.Y = worldPoint.y - camera.pos.y;

            // Invert y-coordinate (todo: add support for sharpener)
            // current issue: the text is still not matching up (too far below the target)
            if (Globals.sharpenerEnabled)
                screenPlayerPoint.Y = Globals.sharpenerResolution.y - screenPlayerPoint.Y;
            else
                screenPlayerPoint.Y = camera.sSize.y - screenPlayerPoint.Y;

            return screenPlayerPoint;*/
        }
    }
}
