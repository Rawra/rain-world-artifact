﻿using RWCustom;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace RWArtifact.Extensions
{
    public static partial class Vector2Extensions
    {
        /// <summary>
        /// Convert a given UnityEngine.Vector2 to a System.Numerics.Vector2
        /// (safe-variant, alloc + copy-based)
        /// </summary>
        /// <param name="v"></param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static Vector2 ToNumericsVector2Safe(this UnityEngine.Vector2 v)
        {
            return new Vector2(v.x, v.y);
        }

        /// <summary>
        /// Convert a given UnityEngine.Vector2 to a System.Numerics.Vector2
        /// (nearly zero-overhead, by-value argument)
        /// </summary>
        /// <param name="v"></param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public unsafe static Vector2* ToNumericsVector2PtrByValue(this UnityEngine.Vector2 v)
        {
            return (Vector2*)&v;
        }

        /// <summary>
        /// Convert a given UnityEngine.Vector2 to a System.Numerics.Vector2
        /// (zero-overhead, by-ref argument; unless JIT ignores inlining)
        /// </summary>
        /// <param name="v"></param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public unsafe static Vector2* ToNumericsVector2PtrByRef(this ref UnityEngine.Vector2 v)
        {
            return (Vector2*)Unsafe.AsPointer(ref v);
        }

        /// <summary>
        /// Convert a given UnityEngine.Vector2 to a System.Numerics.Vector2
        /// (nearly zero-overhead, by-value argument)
        /// </summary>
        /// <param name="v"></param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public unsafe static ref Vector2 ToNumericsVector2RefByValue(this UnityEngine.Vector2 v)
        {
            return ref Unsafe.AsRef<Vector2>(&v);
        }

        /// <summary>
        /// Convert a given UnityEngine.Vector2 to a System.Numerics.Vector2
        /// (zero-overhead, by-ref argument; unless JIT ignores inlining)
        /// </summary>
        /// <param name="v"></param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public unsafe static ref Vector2 ToNumericsVector2RefByRef(this ref UnityEngine.Vector2 v)
        {
            return ref Unsafe.AsRef<Vector2>(Unsafe.AsPointer(ref v));
        }
    }
}
