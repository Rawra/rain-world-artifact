﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
namespace RWArtifact.Compat
{
    internal sealed class CompatSharpener
    {
        // Sharpener-compat settings
        public static bool SharpenerEnabled = false;
        public static Vector2 SharpenerResolution = Vector2.zero;
        public static Vector2 SharpenerFakeRes = Vector2.zero;
    }
}
