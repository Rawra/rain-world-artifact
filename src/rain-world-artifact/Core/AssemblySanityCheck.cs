﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
namespace RWIMGUI.Core
{
    /// <summary>
    /// Helper class to identify potential assembly referencing issues which might lead to
    /// TypeLoadExceptions down the line.
    /// </summary>
    internal class AssemblySanityChecker
    {
        // local = H:\SteamLibrary\steamapps\workshop\content\312520
        // remote = H:\SteamLibrary\steamapps\common\Rain World\RainWorld_Data\StreamingAssets\mods\rwimgui\plugins

        private Dictionary<string, string> assemblyNames;
        private Dictionary<string, List<string>> duplicateAssembliesEx;
        public AssemblySanityChecker()
        {
            assemblyNames = new Dictionary<string, string>();
            duplicateAssembliesEx = new Dictionary<string, List<string>>();
        }

        public void Check()
        {
            Console.WriteLine("AssemblySanityCheck - starting...");
            string executableHostPath = Path.GetDirectoryName(System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName);
            CheckLocalMods(executableHostPath);
            CheckRemoteWorkshop(executableHostPath);
            PrintResults();
            Console.WriteLine("AssemblySanityCheck - finished...");
        }

        /// <summary>
        /// Check the local mod installation directory
        /// </summary>
        /// <param name="executableHostPath"></param>
        private void CheckLocalMods(string executableHostPath)
        {
            if (!Directory.Exists(executableHostPath))
            {
                Console.WriteLine($"Failed to find local file path (CLR host): [{executableHostPath}]");
                return;
            }
            string localModsPath = Path.Combine(executableHostPath, "RainWorld_Data/StreamingAssets/mods");
            string localModFileName = string.Empty;
            foreach (string file in Directory.GetFiles(executableHostPath, "*.dll", SearchOption.AllDirectories))
            {
                localModFileName = Path.GetFileName(file);
                if (assemblyNames.ContainsKey(localModFileName))
                {
                    if (duplicateAssembliesEx.ContainsKey(localModFileName))
                        duplicateAssembliesEx[localModFileName].Add(file);
                    else
                        duplicateAssembliesEx.Add(localModFileName, new List<string>() { file });
                    continue;
                }
                assemblyNames.Add(localModFileName, file);
            }
        }

        /// <summary>
        /// Check the remote steam workshop mod directory
        /// </summary>
        /// <param name="executableHostPath"></param>
        private void CheckRemoteWorkshop(string executableHostPath)
        {
            string remoteFilePath = Path.Combine(Path.GetPathRoot(executableHostPath), "SteamLibrary/steamapps/workshop/content/312520");
            if (!Directory.Exists(remoteFilePath))
            {
                Console.WriteLine($"Failed to find remote steam workshop path: [{remoteFilePath}]");
                return;
            }

            string localModFileName = string.Empty;
            foreach (string file in Directory.GetFiles(remoteFilePath, "*.dll", SearchOption.AllDirectories))
            {
                localModFileName = Path.GetFileName(file);
                if (assemblyNames.ContainsKey(localModFileName))
                {
                    if (duplicateAssembliesEx.ContainsKey(localModFileName))
                        duplicateAssembliesEx[localModFileName].Add(file);
                    else
                        duplicateAssembliesEx.Add(localModFileName, new List<string>() { file });
                    continue;
                }
                assemblyNames.Add(localModFileName, file);
            }
        }

        private void PrintResults()
        {
            StringBuilder sb = new StringBuilder();
            foreach (KeyValuePair<string, List<string>> pair in duplicateAssembliesEx)
            {
                sb.Append("[DUPLICATE-ASSEMBLY]: ");
                sb.Append(pair.Key);
                sb.Append(" -> {\n");
                if (assemblyNames.TryGetValue(pair.Key, out string originalAssembly))
                {
                    sb.Append(originalAssembly);
                    sb.Append("\n");
                }
                foreach (var dup in pair.Value)
                {
                    sb.Append(dup);
                    sb.Append("\n");
                }
                sb.Append("}\n");
            }
            Console.WriteLine(sb.ToString());
        }


    }
}
