﻿using BepInEx;
using HarmonyLib;
using RWCustom;
using System;
using Unity.Collections;
using System.Reflection;
using BepInEx.Bootstrap;
using UnityEngine;
using RWArtifact.Compat;
using RWArtifact.Core;
namespace RWArtifact
{
    [BepInDependency("pjb3005.sharpener", BepInDependency.DependencyFlags.SoftDependency)]
    [BepInPlugin(PLUGIN_GUID, PLUGIN_NAME, PLUGIN_VERSION)]
    public class Plugin : BaseUnityPlugin
    {
        public const string PLUGIN_GUID = "rwartifact";
        public const string PLUGIN_NAME = "Artifact Library";
        public const string PLUGIN_VERSION = "1.0.0";

        private unsafe void Awake()
        {
            try
            {
                Globals.mls = Logger;
                Logger.LogInfo($"Plugin {PLUGIN_NAME} is loading!");

                // On.Hooks Initialization
                On.RainWorld.OnModsInit += RainWorld_OnModsInit;

                // Plugin startup logic
                Logger.LogInfo($"Plugin {PLUGIN_NAME} is loaded!");
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.ToString());
            }
        }

        /// <summary>
        /// Callback for mod initialization (hook setup stage)
        /// </summary>
        /// <param name="orig"></param>
        /// <param name="self"></param>
        private void RainWorld_OnModsInit(On.RainWorld.orig_OnModsInit orig, RainWorld self)
        {
            try
            {
                Logger.LogInfo("RainWorld_OnModsInit:");

                // Mod-compat: Sharpener
                if (BepInEx.ModTools.TryGetMod("pjb3005.sharpener", out PluginInfo plugin))
                {
                    Logger.LogInfo("Enabling compat: Sharpener");
                    CompatSharpener.SharpenerEnabled = true;

                    IntVector2 realRes = (IntVector2)plugin.Instance.GetType().GetField("_realRes", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(plugin.Instance);
                    IntVector2 fakeRes = (IntVector2)plugin.Instance.GetType().GetField("_gameRes", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(plugin.Instance);

                    Logger.LogInfo($"_realRes value: {realRes.ToVector2().ToString()}");
                    CompatSharpener.SharpenerResolution = realRes.ToVector2();

                    Logger.LogInfo($"_gameRes value: {fakeRes.ToVector2().ToString()}");
                    CompatSharpener.SharpenerFakeRes = fakeRes.ToVector2();
                }

            }
            catch (Exception ex)
            {
                Logger.LogError("RainWorld_OnModsInit Error: " + ex.ToString());
            }
            finally
            {
                orig.Invoke(self);
            }
        }
    }
}
