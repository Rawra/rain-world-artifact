# rain-world-artifact

<img src="https://gitlab.com/Rawra/rain-world-artifact/-/raw/main/docs/logo/logo.png" alt="showcase-1" width="600"/>

## What it is

This is a common library used by most of my mods.

## Features

- UnityEngine.Vector2 Extensions:
- - WorldToScreen(ref RoomCamera)
- - ToNumericsVector2()
- BepInEx
- - IsModLoaded(string modGUID)
- - TryGetMod(string modGUID, out PluginInfo plugin)

## Mod Compatability

- Compatible with Sharpener

## Missing references

This project includes a script to automatically fix missing references added via HintPath in the csproj files.
Just run "update-hintpaths-auto.ps1" and make sure the gameinstalldir.txt contains your correct path to Rain World.

You might want to do this before trying to open the project or compiling.

## Compiling: 

The sln is provided and should be enough to compile the project